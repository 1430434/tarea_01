Cadena str;
Boton b1, b2, b3, b4, b5, b6, b7;
PFont p;
String men, men2, men3, men4, men5, men6, men7;


public void setup(){
    size(420,200);
    smooth();
    str = new Cadena("");
    p = createFont("Arial",18);
    textFont(p);
    b1 = new Boton(20,25,"Invierte");
    b2 = new Boton(150,25,"Borrar inicio");
    b3 = new Boton(280,25,"Borrar final");
    b4 = new Boton(20,60,"Llenar");
    b5 = new Boton(150,60,"Vaciar");
    b6 = new Boton(280,60,"Agregar inicio");
    b7 = new Boton(20,95,"Agregar final");

    men ="Aqui va el mensaje";
    men2 = "";
    men3 = "";
    men4 = "";
    men5 = "";
    men6 = "";
    men7 = "";
    
}

public void draw(){
     background(127);
     fill(0,255,0);
     text(str.toString(),20,height/2);
     b1.dibujar();
     b2.dibujar();
     b3.dibujar();
     b4.dibujar();
     b5.dibujar();
     b6.dibujar();
     b7.dibujar();
     fill(255);
     text(men,20,height-30);
     text(men2,20,height-30);
     text(men3,20,height-30);
     text(men4,20,height-30);
     text(men5,20,height-30);
     text(men6,20,height-30);
     text(men7,20,height-30);
}

public void mousePressed(){
    men = b2.click(mouseX, mouseY);
    men2 = b1.click(mouseX, mouseY);
    men3 = b3.click(mouseX, mouseY);
    men4 = b4.click(mouseX, mouseY);
    men5 = b5.click(mouseX, mouseY);
    men6 = b6.click(mouseX, mouseY);
    men7 = b7.click(mouseX, mouseY);
    
}